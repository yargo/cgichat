# CGI chat

simple CGI chat script

## Installation

- put it somewhere in the web server path where it can be executed
- add a writable file `chatfile.txt` (or whatever it is called at the beginning of the CGI script)
- if desired, add `.htaccess` and `.htpasswd` as needed
- enjoy

## Notes

- users are identified by the content of the environment variable `REMOTE_USER`, or if that is empty, by `REMOTE_ADDR`
- there is a possibility to send an e-mail to a predefined address by entering a "magic text" -- please read the source, if you are interested in that!
