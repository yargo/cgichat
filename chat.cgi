#!/bin/sh
myself=`basename $0`
chatf=chatfile.txt
tmpf=${TMP:-/tmp}/$myself$$.tmp
#mailer='mail -s _subject_ john@example.com'
mailer='true'
toplines=5
bottoms=20
if test "$myself" = "chatshort.cgi"
then
toplines=5
bottoms=2
fi
longbottom=1000
mailbottom=50

tr '+;&' ' 
' | sed -e "s/%C2%B0/deg/g;s/%B0/deg/g;
 s/%C3%A4/ae/g;s/%C3%B6/oe/g;s/%C3%BC/ue/g;
 s/%2B/+/g;s/%22/\"/g;s/%26/&/g;s/%2F/\//g;s/%28/(/g;s/%29/)/g;s/%3D/=/g;
 s/%3F/?/g;s/%27/'/g;s/%5E/^/g;s/%7E/~/g;s/%3C/</g;s/%3E/>/g;
 s/%7B/{/g;s/%7D/}/g;s/%5B/[/g;s/%5C/\//g;s/%5D/]/g;s/%21/!/g;s/%24/\$/g;
 s/%2C/,/g;s/%3B/;/g;s/%3A/:/g;s/%23/#/g;s/%7C/|/g;s/%60/'/g;s/%40/@/g;
 s/%25/%/g" >$tmpf
inpt=`cat $tmpf`

nick=${REMOTE_USER:-$REMOTE_ADDR}
datim=`date "+(%m-%d,%H:%M)"`

cat <<EOH
Content-type: text/html

<html><head>
EOH

if test "$QUERY_STRING" = "autopoll=yes"
then echo '<meta http-equiv="refresh" content="9">'
fi

cat <<EOH
<title>chat</title></head><body>
<pre>
`tail -n $toplines $chatf`
</pre><i>now:$datim</i>
<form method="post" action="$myself">
<input name="say" type="text" size="50" maxlength="200">
<input type="submit" value="!">
</form>
<pre>
EOH

case $inpt in
 long=yes)
  tail -n $longbottom $chatf
  ;;
# say=xxxmail)
#  tail -n $mailbottom $chatf | $mailer
#  echo 'mail sent'
#  echo
#  tail -n $bottoms $chatf
#  ;;
 say=*)
  if test "$inpt" != "say="
  then
   line="$nick: ${inpt#say=}"
   line="$datim $line"
   if test "$line" != "`tail -n 1 $chatf`"
   then echo "$line" >>$chatf
   fi
  fi
  tail -n $bottoms $chatf
  ;;
 *)
  cat <<EOH
Just write into the text entry field and click on "!" or press Enter key!
An empty text field will simply reload the display.
"($longbottom)" displays the last $longbottom lines.
"autopoll" activates automatic polling of the display about every ten seconds.
 Before entering text, press "!" once to stop this;
 otherwise, your entered text may be cleared upon reloading.
EOH
  ;;
esac

cat << EOF
</pre>
<form method="post" action="$myself">
<input name="long" type="hidden" value="yes">
<input type="submit" value="($longbottom)">
</form>
<form method="get" action="$myself">
<input name="autopoll" type="hidden" value="yes">
<input type="submit" value="autopoll">
</form>
</body></html>
EOF

/bin/rm -f $tmpf
